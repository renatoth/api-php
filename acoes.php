<?php
    include 'util.php';
    
    header("Content-type: text/plain");
    
	// turn on in production, off for debugging
	libxml_use_internal_errors(false);
	
	// params	
	$acao = isset($_GET['a'])?$_GET['a']:'';
	$tipo = isset($_GET['t'])?$_GET['t']:'';
    
	$url = 'https://statusinvest.com.br/category/advancedsearchresult?CategoryType=1&search={"Sector":"","SubSector":"","Segment":"","my_range":"0;25","dy":{"Item1":null,"Item2":null},"p_L":{"Item1":null,"Item2":15},"p_VP":{"Item1":null,"Item2":2},"p_Ativo":{"Item1":null,"Item2":null},"margemBruta":{"Item1":null,"Item2":null},"margemEbit":{"Item1":null,"Item2":null},"margemLiquida":{"Item1":null,"Item2":null},"p_Ebit":{"Item1":null,"Item2":null},"eV_Ebit":{"Item1":null,"Item2":null},"dividaLiquidaEbit":{"Item1":null,"Item2":null},"dividaliquidaPatrimonioLiquido":{"Item1":null,"Item2":null},"p_SR":{"Item1":null,"Item2":null},"p_CapitalGiro":{"Item1":null,"Item2":null},"p_AtivoCirculante":{"Item1":null,"Item2":null},"roe":{"Item1":null,"Item2":null},"roic":{"Item1":null,"Item2":null},"roa":{"Item1":null,"Item2":null},"liquidezCorrente":{"Item1":null,"Item2":null},"pl_Ativo":{"Item1":null,"Item2":null},"passivo_Ativo":{"Item1":null,"Item2":null},"giroAtivos":{"Item1":null,"Item2":null},"receitas_Cagr5":{"Item1":null,"Item2":null},"lucros_Cagr5":{"Item1":null,"Item2":null},"liquidezMediaDiaria":{"Item1":2000000,"Item2":null}}';
	
	$mainArray = [];
    
    if($acao == ''){
        $fileCache = new FileCacheUtil("acoes/fundamentos", "_ALL");
        if($fileCache->exists() && $fileCache->tsModified() < 60 * 60 * 8){ // cache de 8hs
            $mainArray = json_decode($fileCache->get());
            $jsonOut = json_encode($mainArray, JSON_PRETTY_PRINT);
        } else {
            $mainArray = getJSON($url);
            
            // ordena por p_VP e salva individualmente
            usort($mainArray, function ($a, $b) {
                $a->date = date("Y-m-d H:i", strtotime(date('Y-m-d H:i')));  // adiciona a data
                (new FileCacheUtil("acoes/fundamentos", $a->ticker))->put(json_encode($a));
                return $a->p_VP > $b->p_VP;
            });
            
            $jsonOut = json_encode($mainArray, JSON_PRETTY_PRINT);
            
            if(!$fileCache->exists()){
                $fileCache->put($jsonOut);
            }
        }
    } else {    
        $acao = strtoupper($acao);
        
        if($tipo == 'f'){
            $mainArray = json_decode((new FileCacheUtil("acoes/fundamentos", $acao))->get());            
        } else {
            $tickerCache = new FileCacheUtil("acoes/ticker", $acao);
            
            if($tickerCache->exists() && $tickerCache->tsModified() < 60 * 5){ // cache de 5min
                $mainArray = json_decode($tickerCache->get());
            } else {
                $mainArray = getJSON("http://cotacao.b3.com.br/mds/api/v1/DailyFluctuationHistory/".$acao);
                $jsonOut = end($mainArray->TradgFlr->scty->lstQtn); // ultimo valor
                $jsonOut->date = $mainArray->TradgFlr->date; // adiciona data
                $tickerCache->put(json_encode($jsonOut, JSON_PRETTY_PRINT));
                $mainArray = $jsonOut;
            }            
        }
        $jsonOut = json_encode($mainArray, JSON_PRETTY_PRINT);
    }
    
    echo $jsonOut;
?>