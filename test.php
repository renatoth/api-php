<?php
    //include 'util.php';
    
    header("Content-type: text/plain");
    
	// turn on in production, off for debugging
	libxml_use_internal_errors(false);	
    
    $url = isset($_GET['url'])?$_GET['url']:'';

    $options  = array('http' => array('user_agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36'));
    $context  = stream_context_create($options);    
    
    function curl_get_contents($url){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
        //$html = curl_exec($ch);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }

    echo "file_get_contents:\n".file_get_contents($url, false, $context);
    echo "\n--------------------------------------------------------------\n";
    echo "curl_get_contents:\n".curl_get_contents($url);
?>