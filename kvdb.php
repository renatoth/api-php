<?php
	include 'util.php';
	
	header("Content-type: text/plain");
	
	if(!isset($_GET['t'])){		
		echo "PARAMS:\n";
		echo "- t: tabela\n";		
		echo "- k: key \n";
		echo "- v: value \n";
		echo "\n\n";		
		exit();
	}
	
	// obtem os valores
	$_t = isset($_GET['t'])?$_GET['t']:'';	
	$_k = isset($_GET['k'])?$_GET['k']:'';
	$_v = isset($_GET['v'])?$_GET['v']:'';

	$fcu = new FileCacheUtil($_t, $_k);
	if($_v == ''){
		echo $fcu->get();
	} else if($_v == '*DELETE*'){
		$fcu->delete();
	} else {
		$fcu->put($_v);
	}
?>